import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class JavaStreamExample {
    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>();


//        int a =7;
//        a++;
//        System.out.println(a);

        productList.add(new Product(1, "HP LAPTOP", 25000f));
        productList.add(new Product(2, "ACER LAPTOP", 5000f));
        productList.add(new Product(3, "APPLE LAPTOP", 15000f));
        productList.add(new Product(4, "SAMSUNG LAPTOP", 23000f));
        productList.add(new Product(5, "ASUS LAPTOP", 2000f));


        List <Float> productPriceList = productList.stream().filter(p->p.price >3000).map(p -> p.price).collect(Collectors.toList());
        productPriceList = productPriceList.stream().map(e-> e+1000.0f).collect(Collectors.toList());

        System.out.println(productPriceList);
    }
}
