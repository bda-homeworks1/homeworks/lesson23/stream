import model.User;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    static long f =1;

    public static void fac(int a){
        if(a>1){
            f*=a;
            fac(a-1);
        }else {
            System.out.println(f);
            f=1;
        }

    }

    public static boolean isPrime(int a ){
        if (a==2 || a==3 || a==5){
            return true;
        }else if(a>5 && a%2!=0 && a%3!=0 && a%5!=0){
        return true;
        }
        else {
            return false;
        }

    }


    public static void main(String[] args) {
//        ArrayList<Integer> numList = new ArrayList<>(Arrays.asList(7,4,2,6,22,4,1));
//        ArrayList<Integer> primeList = new ArrayList<>(Arrays.asList(2,3,7,	11,	13,	17,	19,	23,	29,	31,37,41,43,47));
//        System.out.println("min");
//        System.out.println(numList.stream().min(Integer::compare).get());
//        System.out.println();
//
//        System.out.println("maks");
//        System.out.println(numList.stream().max(Integer::compare).get());
//        System.out.println();
//
//        System.out.println("sum");
//        System.out.println(numList.stream().reduce(0,(a,b)-> a+b));
//        System.out.println();
//
//        System.out.println("x*x");
//        numList.stream().forEach(e-> System.out.println(e*e));
//        System.out.println();
//
//
//        System.out.println("x!");
//        numList.stream().forEach(e-> fac(e));
//        System.out.println();
//
//        System.out.println("prime numbers");
//        numList.stream().filter(e-> isPrime(e)).forEach(a-> System.out.println(a));


        ArrayList<User> users = new ArrayList<>();
        users.add(new User("Ali",22));
        users.add(new User("Ahmet",67));
        users.add(new User("Bilal",32));
        users.add(new User("Oktay",20));
        users.add(new User("Hesen",23));
        users.add(new User("Nermin",25));
        users.add(new User("Sebine",24));
        users.add(new User("Aynur",26));
        users.add(new User("Mesme",25));


        ArrayList<String> usersName = new ArrayList<>();
        System.out.println("A ile baslayan userler");
        users.stream().filter(e->e.name.startsWith("A")).forEach(a-> usersName.add(a.name));
        System.out.println(usersName);
        System.out.println();

        System.out.println("uzunlugu 3-den boyuk olanlar");
        usersName.stream().filter(e-> e.length()>3).forEach(a-> System.out.println(a));
    }
}
